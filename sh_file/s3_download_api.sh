#!/bin/bash
# for userdata

aws="/usr/local/bin/aws --region ap-northeast-1"
work_dir="/data"
s3_url="s3://test-terraform-take/api_server"
logger="logger -t $0"

# -- command --
${logger} "start scirpt."
mkdir -p "${work_dir}"

# -- install awscli --
sudo apt update -y
sudo apt upgrade -y
sudo apt install python3-pip -y
sudo pip3 install -U pip
sudo pip install awscli

# -- download --
${aws} s3 cp --recursive "${s3_url}" "${work_dir}"
${logger} "finished."
exit 0
